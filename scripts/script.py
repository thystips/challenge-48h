import requests
from bs4 import BeautifulSoup
import sqlite3
import time

# ==============================
#           Variables
# ==============================
tm = time.time()

conn = sqlite3.connect('run.db')
cursor = conn.cursor()

cursor.execute('drop table Restaurants;')
cursor.execute('drop table Plats;')
cursor.execute('drop table Avis;')
conn.commit()

cursor.execute('CREATE TABLE Restaurants (id INTEGER  PRIMARY KEY AUTOINCREMENT, type text, nom text, adresse text, prixMoyen text, note text, information text);')
cursor.execute('CREATE TABLE Plats (id INTEGER  PRIMARY KEY AUTOINCREMENT, type text, titre text, prix text, fk_restaurantID integer, FOREIGN KEY(fk_restaurantID) REFERENCES Restaurants(id));')
cursor.execute('CREATE TABLE Avis (id INTEGER  PRIMARY KEY AUTOINCREMENT, content text, note text, pseudo text, date text, fk_restaurantID integer, FOREIGN KEY(fk_restaurantID) REFERENCES Restaurants(id));')
conn.commit()

All = []
headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52',
        'Accept-Encoding': 'gzip, deflate, br',
        'Pragma': 'no-cache',
    }
site = 'https://www.lafourchette.com'
Ok = True
p = 1

# ==============================
#           Fonctions
# ==============================

def getText(txt):
       return txt.get_text()

# ==============================
#             Objets
# ==============================

class Restaurant:
       def __init__(self, type, titre, adresse, prixMoyen, note, information):
              self.type = type
              self.titre = titre
              self.adresse = adresse
              self.prixMoyen = prixMoyen
              self.note = note
              self.information = information
              
       # def __repr__(self):
              # return f'{self.titre} | {self.adresse} [{self.type}]'

class Plat:
       def __init__(self, type, titre, prix):
              self.type = type
              self.titre = titre
              self.prix = prix

       # def __repr__(self):
       #        return f'{self.titre} | {self.prix} [{self.type}]'

class Avis: 
       def __init__(self, pseudo, content, note, date):
              self.pseudo = pseudo
              self.content = contentAvis
              self.note = note
              self.date = date

       # def __repr__(self):
       #        return f'{self.pseudo} | {self.note} [{self.date}]'


# ==============================
#          programmes
# ==============================

if __name__ == '__main__':
       # Chaque sites
       while Ok:
              print(f'Page - {p}')
              req = requests.get(f"https://www.lafourchette.com/search/?cityId=60566&p={p}", headers=headers).text
              if  BeautifulSoup(req, 'html.parser').find_all(attrs={'class': '_2tgfX'}) :
                     Ok = False
              soup = BeautifulSoup(req, 'html.parser').find_all(attrs={'class': 'css-1fzs0zb e19krov40'}) # css-1fzs0zb e19krov40
              for resto in soup:
              # Traitement un site
                     try:
                            typeResto = getText(resto.find_all(attrs={'class': 'css-12uqx9u e5hfpuf0'})[0]).split('}')[-1] # Type du restaurant
                     except Exception as e:
                            typeResto = 'None'
                     try:
                            titreResto = getText(resto.find_all(attrs={"class": 'css-1i5men4 e5hfpuf0'})[0]).replace('"', '').replace("'", "") # Titre du restaurant
                     except Exception as e:
                            titreResto = 'None'
                     try:
                            adresseResto = getText(resto.find_all(attrs={'class': 'css-1qbj1cq e5hfpuf0'})[0]).replace('\n', '').replace('          ', ' ').replace('        ','').replace('"', '').replace("'", "") # Adresse du restaurant
                     except Exception as e:
                            adresseResto = 'None'
                     try:
                            prixMoyenResto = getText(resto.find_all(attrs={'class': 'css-1q630al e5hfpuf0'})[0]).split(' ')[2].replace('\xa0€', '') # Prix moyen du restaurant
                            if ".css-1ti9qu5::after{content:'\xa0•';}" in prixMoyenResto: 
                                   prixMoyenResto = getText(resto.find_all(attrs={'class': 'css-1q630al e5hfpuf0'})[0]).split(' ')[2].replace('\xa0€', '').replace(".css-1ti9qu5::after{content:'\xa0•';}", '')
                     except Exception as e:
                            prixMoyenResto = 'None'
                     try:
                            noteResto = getText(resto.find_all(attrs={'class': 'css-ysgh0a evj35i10'})[0]).split(' /')[0] # Note du restaurant
                     except Exception as e:
                            noteResto = 'None'
                     try:              
                            pageResto = requests.get(site + str(resto.find('a').get('href')), headers=headers).text
                            s = BeautifulSoup(pageResto, 'html.parser').find_all(attrs={'class': '_1Rbw0 _2gz3X _1rEfm _2b_7h pwljc'})
                            infoResto = getText(s[0]).replace('"', '').replace("'", "") # info du restaurant
                     except Exception as e:
                            infoResto = 'None'
                     
                     if titreResto not in cursor.execute(f'select nom from Restaurants').fetchall():
                            cursor.execute(f'insert into Restaurants (type, nom, adresse, prixMoyen, note, information) values ("{typeResto}", "{titreResto}", "{adresseResto}", "{prixMoyenResto}", "{noteResto}", "{infoResto}")')
                            conn.commit()
                            idResto = cursor.execute(f'select id from Restaurants where nom = "{titreResto}"').fetchall()[0][0]
                            print(idResto)

                     # Scrap les menu du restaurant
                     pageMenu = requests.get(site + str(resto.find('a').get('href') + '/menu'), headers=headers).text
                     for types in BeautifulSoup(pageMenu, 'html.parser').find_all(attrs={'class': '_1kLCf'})[0].find_all(attrs={'class': 'css-1iktdca ep9dqpy0'}):
                            typePlat = getText(types.find_all(attrs={'class': 'media-object-section main-section'})[0]).replace('"', '').replace("'", "")
                            for plat in types.find_all(attrs={'class': 'css-5k0l4q ep9dqpy0'}):
                                   try:
                                          titrePlat  = getText(plat.find(attrs={'class': 'css-e6kqne e5hfpuf0'})).replace('"', '').replace("'", "")
                                   except Exception as e:
                                          titrePlat = 'None'
                                   try: 
                                          prixPlat  = getText(plat.find(attrs={'class': 'css-123xaft e5hfpuf0'})).replace('\xa0€', '').replace('"', '').replace("'", "")
                                   except Exception as e:
                                          prixPlat = 'None'

                                   if titrePlat not in cursor.execute(f'select titre from Plats').fetchall():
                                          cursor.execute(f'insert into Plats (type, titre, prix, fk_restaurantID) values ("{typePlat}", "{titrePlat}", "{prixPlat}", "{idResto}")')
                                          conn.commit()

                     # Scrap les avis du restaurant
                     pageAvis = requests.get(site + str(resto.find('a').get('href') + '/avis'), headers=headers).text
                     for avis in BeautifulSoup(pageAvis, 'html.parser').find_all(attrs={'class': '_1m9v9'}):
                            try:
                                   pseudoAvis =  getText(avis.find_all(attrs={'class': '_1rTLO'})[0]).replace('"', '').replace("'", "")
                            except Exception as e:
                                   pseudoAvis = 'None'
                            try:
                                   contentAvis = getText(avis.find_all(attrs={'class': 'eveXu'})[0]).replace('"', '').replace("'", "")
                            except Exception as e:
                                   contentAvis = 'None'
                            try:
                                   noteAvis = getText(avis.find_all(attrs={'class': '_6BMFC'})[0]).replace('"', '').replace("'", "")
                            except Exception as e:
                                   noteAvis = 'None'
                            try:
                                   dateAvis = getText(avis.find('time')).replace('"', '').replace("'", "")
                            except Exception as e:
                                   dateAvis = 'None'
                            if pseudoAvis not in cursor.execute(f'select pseudo from Avis').fetchall():
                                   cursor.execute(f'insert into Avis (content, note, pseudo, date, fk_restaurantID) values ("{contentAvis}", "{noteAvis}", "{pseudoAvis}", "{dateAvis}", "{idResto}")')
                                   conn.commit()
              p += 1

       print('========================================')
       print('              Script fini :)              ')
       print('========================================')
       print(f'\nTemps d\'éxécution: {time.time() - tm}')