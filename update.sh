#!/bin/bash
echo -e "$ git pull"
git pull
echo -e "$ git submodules sync --recursive"
git submodule sync --recursive
echo -e "$ git submodule update --remote --recursive --rebase --init (La réalisation de cette étape peut prendre un peu de temps)"
git submodule update --remote --recursive --rebase --init

git submodule foreach git checkout master
